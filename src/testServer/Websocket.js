const http = require('http');
const express = require('express');
const router = express.Router();
const WebSocketServer = require('websocket').server;
const pubMQTT = require('./pubMQTT'); // Importieren Sie die pubMQTT-Klasse
const { constrainedMemory } = require('process');
let proofBarcode = 0;
let Barcode;
const mqttPublisher = new pubMQTT();

router.post('/packageNumber', (req, res) => {
    
    Barcode = req.body;
    if(proofBarcode != Barcode)
    {
        mqttPublisher.publish('Barcode',Barcode);
        console.log("Barcode: ", Barcode);
        proofBarcode = Barcode;
    }
    console.log("Barcode von http", Barcode);
        res.status(201).json({ success: "User added"  });
        console.log("Erfolgreich Barcode erhlaten");
    });


const startWebSocketServer = () => {
    const webSocketServerPort = 8111;
    const server = http.createServer();
    server.listen(webSocketServerPort);
    console.log('WebSocket Server is listening to Port 8111');

    const wsServer = new WebSocketServer({
        httpServer: server,
        autoAcceptConnections: true
    });

    const clients = {};

    const getUniqueID = () => {
        const s4 = () => Math.floor((1+Math.random()) * 0x10000).toString(16).substring(1);
        return s4() + s4() + '-' + s4();
    };

    

    wsServer.on('request', function (request) {
        console.log('message');
        var userID = getUniqueID();
        console.log((new Date()) + 'Received a new connection from ' + request.origin + '.');
        console.log('message');
        const connection = request.accept(null, request.origin);
        clients[userID] = connection;

        connection.on('message', function(message) {
            if (message.type === 'utf8') { // accept only text
                console.log('Received Message MQTT: ', message.utf8Data);

                const data = JSON.parse(message.utf8Data);
                console.log('Received Message MQTT nach dem parsen: ', data);
                Barcode = data.packageNumber.toString();
                const statusDoor = data.statusDoor;
                const statusLight= data.statusLight;
                const inputStatus = JSON.stringify({ statusDoor, statusLight });
                //mqttPublisher.publish('Inputs', inputStatus);

                

               
                

                // broadcasting message to all connected clients
                for(key in clients) {
                    clients[key].sendUTF(message.utf8Data);
                    //console.log('sent Message to: ', message.utf8Data);
                }
            }
        });

        connection.on('close', function(connection) {
            console.log((new Date()) + " Peer " + userID + " disconnected.");
            // remove user from the list of connected clients
            delete clients[userID];
        });
    });
}

module.exports = {startWebSocketServer, router};
const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const cors = require('cors');
const pubMQTT = require('./pubMQTT');
const { startWebSocketServer } = require('./Websocket'); // Importieren Sie die Funktion



const app = express();
const port = 4001;

app.get('/api/data', (req, res) => {
  res.json({ message: 'Hello, world!' });
});


app.use(bodyParser.json());
app.use(cors());
app.use(express.json());

const { router } = require('./Websocket');
app.use('/api/add', router);

app.get('/', (req, res) => {
  res.send('Hello, this is your Express server and Lukas stinkt :)');
});

app.get('/api', (req, res) => {
  res.json({ "message": "Hello" });
  console.log('Sent Hello world');
});

app.listen(port, () => {
    console.log(`[${new Date().toISOString()}] Server is running at port: ${port}`);
  startWebSocketServer();
});
import { w3cwebsocket as W3CWebSocket } from 'websocket';
import React, { Component, useState, useEffect} from 'react';
import ThreeButtons from './components/Buttons';
import Lights from './components/Light';
import { ThemeProvider } from '@mui/material/styles';
import Button from "@mui/material/Button";
import { Grid } from '@mui/material';
import { styled } from '@mui/material/styles';

import "./App.css";

// app.js



function App() {
  const [statusDoor, setStatusDoor] = useState(0);
const [statusLight, setStatusLight] = useState(0);
const [packageNumber, setPackageNumber] = useState(null);
const [submitStatus, setSubmitStatus] = useState(null);

  

const client = new W3CWebSocket('ws://localhost:8111');

client.onopen = () => {
  console.log('WebSocket Client Connectedhier');
};



client.addEventListener('open', function (event) {
  console.log(" Der WebSocket ist jetzt bereit, Nachrichten zu senden");
});

const handleStatusDoorChange = async (newStatusDoor) => {
  if (newStatusDoor === statusDoor) {
    return;
  }

  setStatusDoor(newStatusDoor);

  if (client.readyState === WebSocket.OPEN) {
    client.send(JSON.stringify({ statusDoor: newStatusDoor.toString(), packageNumber }));
  }
  else{
    console.log("Keine Verbindung mit dem Websocket");
  }
};

const handleStatusLightChange = async (newStatusLight) => {
  if (newStatusLight === statusLight) {
    return;
  }

  setStatusLight(newStatusLight);

  if (client.readyState === WebSocket.OPEN) {
    client.send(JSON.stringify({ statusLight: newStatusLight.toString(), packageNumber}));
  }
  else{
    console.log("Keine Verbindung mit dem Websocket");
  }
};

const handlePackageNumberChange = (newPackageNumber) => {
  setPackageNumber(newPackageNumber);

  if (client.readyState === WebSocket.OPEN) {
    client.send(JSON.stringify({ packageNumber: newPackageNumber }));
    console.log("Gesendete Barcode", newPackageNumber);
  } else {
    console.log("Keine Verbindung mit dem Websocket");
  }
};



 

client.addEventListener('message', function (event) {
  console.log('Message from server: ', event.data);
});

client.addEventListener('open', function (event) {
  console.log('Connected to server');
});

client.addEventListener('error', function (event) {
  console.error('Error connecting to server:', event);
});

client.addEventListener('close', function (event) {
  console.log('Connection to server closed');
});

client.onerror = function(event) {
  console.error("WebSocket error observed:", event);
};


return (
  <div className="App">
    <Grid container spacing={5}>
      <Grid item lg={6} >
        <div>
          <Lights statuslight={statusLight} statusdoor={statusDoor}></Lights>
        </div>
      </Grid>
      <Grid item lg={6} >
      <ThreeButtons onStatusDoorChange={handleStatusDoorChange} onStatusLightChange={handleStatusLightChange} onPackageNumberChange={handlePackageNumberChange} /> 
      </Grid>
    </Grid>
  </div>
);
}

export default App;
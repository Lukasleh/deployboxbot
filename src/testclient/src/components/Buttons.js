//Taster passt paket rein 

import React, {useState, useCallback, useEffect} from 'react';
import { Button, Grid, Chip, Switch, TextField } from '@mui/material';
import "./ContainerEingaenge.css";
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import FormHelperText from '@mui/material/FormHelperText';

function ThreeButtons({ onStatusLightChange, onStatusDoorChange, onPackageNumberChange}) {
  const [StatusDoor, setStatusDoor] = useState(0);
  const [StatusLight, setStatusLight] = useState(0);
  const [PackageNumber, setPackageNumber] = useState(0);
  const [submitStatus, setSubmitStatus] = useState(null);
  const [StatusPackageError, setStatusPackageError] = useState(0);

  

    const handleSubmit = (event) => {
      fetch('/api/add/packageNumber', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ PackageNumber: parseInt(PackageNumber) }),
    })
    .then(response => {
        if (response.ok) {
            console.log('Success:', response);
            // Ladescreen ausblenden
        } else {
            console.error(`HTTP error! status: ${response.status}`);
            // Ladescreen ausblenden
        }
    })
      event.preventDefault();
      onPackageNumberChange(PackageNumber);
      console.log("Packtenummer",PackageNumber);

      // Ladescreen anzeigen
      setSubmitStatus("Laden...");

    }
 
  const handleSwitchChange = (event) => {
    const newStatusDoor = event.target.checked ? 1 : 0; // Definiere newStatusDoor
    setStatusDoor(newStatusDoor);
    onStatusDoorChange(newStatusDoor);
  };


  const handleLightOn = () => {
    setStatusLight(1); // Setze StatusLight auf 1, wenn der "Licht an" Button geklickt wird
    onStatusLightChange(1); // Informiere die übergeordnete Komponente über die Änderung
     // Führe die fetch-Anfrage aus
  };
  
  const handleLightOff = () => {
    setStatusLight(0); // Setze StatusLight auf 0, wenn der "Licht aus" Button geklickt wird
    onStatusLightChange(0); // Informiere die übergeordnete Komponente über die Änderung
     // Führe die fetch-Anfrage aus
  };

  const handleErrorPackageOn = () => {
    setStatusPackageError(1); // Setze StatusLight auf 1, wenn der "Licht an" Button geklickt wird
    setTimeout(() => {
      setStatusPackageError(0);
    }, 60000); // 60000 Millisekunden sind 60 Sekunden
  };
  


  const label = {
    text: "Mein Switch",
    color: "primary",
    fontSize: 16,
    
  };

  const resetForm = () => {
    setPackageNumber("");
    setSubmitStatus(null);
    
}


  return (
    <div className='container-input'>
      <h1>Eingänge</h1>
      <Grid item>
        <Button variant="contained" style={{ backgroundColor: 'green', color: 'white', margin: "10px" }} onClick={() => handleLightOn("Neuer Wert")}>
          Licht an 
        </Button>
        <Button variant="contained" style={{ backgroundColor: 'crimson', color: 'white', margin: "10px" }} onClick={handleLightOff}>
          Licht aus 
        </Button>
      </Grid>
      <Grid item>
        <p1>Türöffner</p1>
        <Switch {...label} defaultChecked  onChange={  handleSwitchChange }/>
      </Grid>
      <Grid item > <Button variant="contained" style={{ backgroundColor: 'red', color: 'white', margin: "10px" }} onClick={handleErrorPackageOn}>
          Paket zu groß oder beschädigt
        </Button></Grid>
      <Grid>
      {submitStatus=== null ? (
        <FormControl onSubmit={handleSubmit}>
          <FormLabel>Paket anliefern Barcode Nummer</FormLabel>
          <TextField label="Paket-Nummer" type="text" color='primary'  value={PackageNumber} onChange={e => setPackageNumber(e.target.value)}/>
          <Button type="submit" variant="contained" color="primary" onClick={handleSubmit} disabled={!PackageNumber}>Paket anliefern</Button>
          </FormControl>
           ): (
                <div>
                     {submitStatus}
                <Button variant="contained" color="primary" onClick={resetForm}>Neues Paket</Button>
                </div>
            )
            }
      </Grid>
    </div>
  );
}

export default ThreeButtons;

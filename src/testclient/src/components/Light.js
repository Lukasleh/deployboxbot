import React, { useState} from "react";
import "./ContainerAusgaenge.css";
import Button  from "@mui/material/Button";

function Lights({statuslight, statusdoor}) {//Man braucht geschweifte Klammern um die props richtig zu übergeben 
    
    console.log(statuslight);
    return(
        <div className="container-output">
            <h1>Ausgänge</h1>
            <p1> Status Licht {statuslight}</p1>
            <div className={`light ${statuslight === 1 ? 'an' : ""}`}> </div>
            <p1> Status Türe {statusdoor}</p1>
            <div className={`light ${statusdoor === 1 ? 'an' : ""}`}>  
            </div>
        </div>
            

    );
    
}

export default Lights;
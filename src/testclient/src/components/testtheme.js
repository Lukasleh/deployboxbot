// theme.js

import { createTheme } from '@mui/material/styles';
import { amber, orange } from '@mui/material/colors';

const theme = createTheme({
    palette: {
        primary: amber,
        secondary: orange,
      },
  // Weitere Theme-Einstellungen hier
  components:{
    MuiButton:{
      styleOverrides:{
        root:{
          borderRadius: 15,       //aplies on all MuiButtons
        },
        outlinedPrimary:{       //aplies only to all outlinedPrimary Buttons
          borderRadius: 50,
          backgroundColor: '#000000'
        }
      }
    }
  }
});

export default theme;

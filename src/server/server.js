const express = require('express');
const path = require('path');
const isCurrentTimeInRange = require('./routes/checkForTime.js')
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const port = 3001;
app.use(express.json()); //check all income request -> convert them to objects
const startWebSocketServer = require('./routes/websocket.js');
const startMqtt = require('./routes/SubMQTT.js');
let start =0;
app.use(express.static(path.join(__dirname, './../client/build')));
const { sendLast5receivedPackages }  = require('./routes/readFromDatabase.js');

app.use (cors());

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

const addToDatabaseRouter = require('./routes/addToDatabase');
app.use('/api/edit', addToDatabaseRouter);

const {router} = require('./routes/readFromDatabase');
app.use('/api/read', router);

const saveTime = require('./routes/Time');
app.use('/api/Time', saveTime);

//Alle Funktionen die gesatrtet werden sobald der server gestartet wird
app.listen(port, async() => {
  console.log(`Server is running at port: ${port}`);
  console.log('Nutzungszeit:  {isCurrentTimeInRange()}');
  if(start === 0)
  {
    DatatransferWS();
  }
  else 
  {
    let statusLightValue = await startMqtt();
    console.log('Received status light value:', statusLightValue);
    await sendLast5receivedPackages(wss, statusLightValue);
  
    console.log('MQTT gestartet, sende letzte 5 empfangene Pakete...');
  }
  
});

async function DatatransferWS() {
  try {
    const wss = await startWebSocketServer();
    console.log('WebSocket-Server gestartet, starte MQTT...');
    
    await sendLast5receivedPackages(wss, null);
    start = 1;

    let statusLightValue = await startMqtt(wss);
    console.log('Received status light value:', statusLightValue);
    await sendLast5receivedPackages(wss, statusLightValue);
  
    console.log('MQTT gestartet, sende letzte 5 empfangene Pakete...');
  } catch (error) {
    console.error('Fehler beim Starten des WebSocket-Servers:', error);
  }
}
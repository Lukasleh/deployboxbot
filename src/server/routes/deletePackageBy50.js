//==================================================================
//
// Datei: function deletePackageBy50.js
//
// Beschreibung: In dieser Datei befindet sich die Funktion: getCountOfReceivedPackages(db) und deleteOldestReceivedPackageIfCountIs50(db)
// Notizen: //
// Autor: Nils Melber
// Version: 1.0
// Stand: 10.01.24 20:00 Uhr
//==================================================================

//===FUNKTION======================================================
// Name: getCountOfReceivedPackages
// Beschreibung: Zählt die Anzahl der empfangenen Pakete in der Datenbank (receiced =1)
// Parameter: "db" ist die Datenbank, auf die zugegriffen wird 
//==================================================================
async function getCountOfReceivedPackages(db) {
        const countQuery = 'SELECT COUNT(*) AS receivedCount FROM packages WHERE received = 1';
        try {
            const row = await db.queryOne('SELECT COUNT(*) AS receivedCount FROM packages WHERE received = 1', []);
            return row ? row.receivedCount : 0;  
        } catch (err) {
            console.error(err.message);
            return 0;
        }
}
//===FUNKTION======================================================
// Name: deleteOldestReceivedPackageIfCountIs50
// Beschreibung: Löscht das erste angelieferte Paket, wenn getCountOfReceivedPackages die Zahl 50 zurückgibt
// Parameter: "db" ist die Datenbank, auf die zugegriffen wird 
//==================================================================
async function deleteOldestReceivedPackageIfCountIs50(db) {
    const receivedCount = await getCountOfReceivedPackages(db);
    console.log(receivedCount);

    if (receivedCount === 50) {
        const selectQuery = 'SELECT MIN(timeStamp) AS minTimeStamp FROM packages WHERE received = 1';
        const row = await db.queryOne(selectQuery, []);

        if (row && row.minTimeStamp !== null) {
            const minTimeStamp = row.minTimeStamp;
            const deleteQuery = 'DELETE FROM packages WHERE timeStamp = ? AND received = 1';
            await db.runQuery(deleteQuery, [minTimeStamp]);
            console.log(`Ältestes Paket mit timeStamp ${minTimeStamp} gelöscht.`);
        } else {
            console.error('Fehler beim Abrufen des ältesten Pakets.');
        }
    }
};
    
module.exports = {
    deleteOldestReceivedPackageIfCountIs50
};
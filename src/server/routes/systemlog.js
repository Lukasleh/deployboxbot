const dbPath = './databaseSwProject.db';
const Database = require("./databaseClass"); 
const { getCurrentTime } = require('./getCurrentTime');

// MQTT-Verbindung konfigurieren
//const mqttBrokerAddress = "your_mqtt_broker_address";
//const mqttTopic = "your_mqtt_topic";

// const client = mqtt.connect(mqttBrokerAddress);

// client.on('connect', () => {
//     client.subscribe(mqttTopic, (err) => {
//         if (err) {
//             console.error(err);
//         } else {
//             console.log('Connected to MQTT broker and subscribed to topic');
//         }
//     });
// });

client.on('message', (topic, message) => {
    // Nachrichtenverarbeitung hier
    const messageString = message.toString();
    const description = getDescriptionFromMessage(messageString);
    const timestamp = getCurrentTime();

    // Neue Zeile in der Datenbank einfügen
    insertLog(description, timestamp);
});

function getDescriptionFromMessage(message) {
    // Logik zum Extrahieren der Beschreibung aus der MQTT-Nachricht
    // Beispiel: Hier könnte eine einfache Zuordnung der Nachricht zur Beschreibung erfolgen
    if (message.includes("Box wird entriegelt")) {
        return "Box wird entriegelt";
    } else if (message.includes("Box wurde geöffnet")) {
        return "Box wurde geöffnet";
    } else if (message.includes("Box wurde geschlossen")) {
        return "Box wurde geschlossen";
    } else if (message.includes("Barcode Scanner wurde betätigt")) {
        return "Barcode Scanner wurde betätigt";
    } else if (message.includes("Fehlertaster wurde betätigt")) {
        return "Fehlertaster wurde betätigt";
    } else {
        return "Unbekannte Beschreibung";
    }
}


// Neue Zeile in der Datenbank einfügen
async function insertLog(description, timestamp) {
        const dbSys = new Database(dbPath);
        const sql = "INSERT INTO systemLog (description, timeStamp) VALUES (?, ?)";
        const params = [description, timestamp];
        await dbSys.runQuery(sql, params);
        await db.closeConnection();
     
}


const mqtt = require('mqtt');
const express = require('express');
const router = express.Router();

class pubMQTT {
  client;

  constructor() {
    // Verbinden Sie sich mit dem MQTT-Broker
    this.client = mqtt.connect('mqtt://localhost:1883', {
      clientId: 'my-publisher',
    });

    // Fügen Sie einen Listener für das Ereignis "connect" hinzu
    this.client.on('connect', () => {
      console.log('MQTT zu verbinden');
    });

    // Fügen Sie einen Listener für das Ereignis "error" hinzu
    this.client.on('error', (err) => {
      console.log('Fehler beim Verbinden mit dem MQTT-Broker:', err);
    });
  }

  publish(topic, data) {
    // Senden Sie eine Nachricht über MQTT
    this.client.publish(topic, JSON.stringify(data));
  }


}

module.exports = pubMQTT;
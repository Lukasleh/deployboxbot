//==================================================================
//
// Datei: isTheDoorClosed.js
//
// Beschreibung: In dieser Datei befindet sich die Funktion: sendMailDoorNotClosed(), beendeProgramm(), tuereClosed() und die definition für die dooOpen E-Mail
// Notizen: //
// Autor: Nils Melber
// Version: 1.0
// Stand: 10.01.24 19:30 Uhr
//==================================================================
const readline = require('readline');
const EmailSender = require('./emailSenderClass'); 

const doorOpen = new EmailSender(
    `<h1>Die tuere ihrer Paketbox ist schon länger als eine Minute geöffnet </h1><p>Die tuere ihrer Paketbox ist schon länger als eine Minute geöffnet </p>`,
    'Die Tuere ist offen!'
);

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

let isJaEntered = false;
//===FUNKTION======================================================
// Name:  sendMailDoorNotClosed
// Beschreibung: Sendet eine Mail, dass die Türe geöffnet ist
//==================================================================
async function sendMailDoorNotClosed() {
    const sendEmailsPromise = () => {
        return new Promise((resolve, reject) => {
            doorOpen.sendEmails()
                .then(() => resolve())
                .catch((e) => reject(e));
        });
    };

    try {
        await sendEmailsPromise();
    } catch (e) {
        console.log(e);
    }
}
//===FUNKTION======================================================
// Name:  finishProgramm
// Beschreibung: Beendet das Programm 
//==================================================================
function finishProgramm() {
  console.log('Das Programm wird beendet.');
  rl.close();
}
//===FUNKTION======================================================
// Name:  tuereClosed
// Beschreibung: Wenn länger als 1 Minute die Türe nicht geschlossen wird, wird die sendMailDoorNotClosed-Funktion aufgerufen
//==================================================================
function tuereClosed(sensorDoor) {//sensorDoor muss übergeben werden
  // Starte das Timeout, wenn keine Eingabe erfolgt
  timeoutId = setTimeout(() => {
  sendMailDoorNotClosed();
  finishProgramm();
  }, 60000);

  rl.question('Türe geschlossen? (Ja) ', (antwort) => {
    clearTimeout(timeoutId); // Lösche das Timeout, wenn "Ja" eingegeben wurde
    if (antwort.toLowerCase() === 'ja') {
      isJaEntered = true;
      finishProgramm();
    } else {
      tuereClosed();  // Starte das Programm erneut, wenn "Ja" nicht eingegeben wurde
    }
  });
}

module.exports = {
 tuereClosed};
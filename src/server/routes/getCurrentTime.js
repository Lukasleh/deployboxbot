//==================================================================
//
// Datei: function getCurrentTime.js
//
// Beschreibung: In dieser Datei befindet sich die Funktion: getCurrentTime()
// Notizen: //
// Autor: Nils Melber
// Version: 1.0
// Stand: 10.01.24 19:30 Uhr
//==================================================================
const moment = require('moment-timezone');
const defaultTimezone = 'Europe/Berlin';

//===FUNKTION======================================================
// Name:  getCurrentTime
// Beschreibung: Gibt die aktuelle Uhrzeit+Datum zurück, aus der Zeitzone, die oben angegeben ist
//==================================================================
function getCurrentTime() {  
  return moment().tz(defaultTimezone);
}

module.exports = {
  getCurrentTime
};
const fs = require('fs');

function isCurrentTimeInRange() {
    const data = fs.readFileSync('./time.config', 'utf8');
    const match = data.match(/Startzeit: (\d+), Endzeit: (\d+)/);
    if (!match) {
        throw new Error('Invalid file format');
    }

    const startTime = parseInt(match[1]);
    const endTime = parseInt(match[2]);
    const currentHour = new Date().getHours();
    if (startTime > endTime) {
        return currentHour >= startTime || currentHour <= endTime;
    } else {
        return currentHour >= startTime && currentHour <= endTime;
    }
}

module.exports = isCurrentTimeInRange;
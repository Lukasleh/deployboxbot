//==================================================================
//
// Datei: PackageProcessIfPackagefound.js 
//
// Beschreibung: In dieser Datei befindet sich die Funktion: processPackageAndCloseDoor(db, code, answer) und die definition für die successfull E-Mail und unsuccessful E-Mail
// Notizen: //
// Autor: Nils Melber
// Version: 1.0
// Stand: 10.01.24 19:15 Uhr
//==================================================================
const { deleteOldestReceivedPackageIfCountIs50 } = require('./deletePackageBy50');
const { tuereClosed } = require('./isTheDoorClosed');
const EmailSender = require('./emailSenderClass');
const { getCurrentTime } = require('./getCurrentTime');
let errorStatus = 0;

const successfull = new EmailSender(
    `<h1>Ihr Paket ist angekommen</h1><p>Ihr Paket ist da und Sie können es aus der Paketbox holen</p>`,
    'Ihr Paket ist angekommen'
);
const unsuccessful = new EmailSender(
    `<h1>Ihr Paket konnte nicht zugestellt werden</h1><p>Ihr Paket konnte nicht zugestellt werden. Entweder ist es zu groß oder es hat andere Mängel.</p>`,
    'Ihr Paket konnte nicht zugestellt werden'
);
//===FUNKTION======================================================
// Name: processPackageAndCloseDoor
// Beschreibung: Wenn "answer" = ja ist, wird eine erfolgreiche Mail gesendet, wenn nicht eine unerfolgreiche. Demenstprechend wird auch der Datenbankeintrag angepasst und dementsprechend  weiter FUnktionen aufgerufen
// Parameter: "db" ist die Datenbank, auf die zugegriffen wird 
// Parameter: "code",  ist die Zahlenkette die von dem Barcode scanner eingelesen wird.
// Parameter: "answer" ist die Antwort die übergeben wurde, ob das Paket in die Box passt und nicht beshcädigt ist
//==================================================================
async function processPackageAndCloseDoor(db, code) {// muss noch ob tuere zu ist hinzufügen
    const Way= await waitForErrorButton();
    const sendEmailsPromise = () => {  // hier wird eine Funktion erstellt, die eine Promise zurückgibt und die Emails sendet, mithilfe der EmailSender Klasse
        return new Promise((resolve, reject) => {
            successfull.sendEmails() 
                .then(() => resolve())
                .catch((e) => reject(e));
        });
    };
    
    const sendUnsuccessfulEmailsPromise = () => {  // hier wird eine Funktion erstellt, die eine Promise zurückgibt und die Emails sendet, mithilfe der EmailSender Klasse
        return new Promise((resolve, reject) => {
            unsuccessful.sendEmails() 
                .then(() => resolve())
                .catch((e) => reject(e));
        });
    };

    try {
        if (Way === 1) {  // Wenn das Paket in die Box passt und nicht beschädigt ist
            const currentTime = getCurrentTime();
            const updateQuery = `UPDATE packages SET received = 1, timeStamp = "${currentTime.format('YYYY-MM-DD HH:mm:ss')}" WHERE paketNumber = ?`;
            await db.runQuery(updateQuery, [code]);
            console.log('Türe erfolgreich geöffnet und received auf 1 gesetzt.');  // "received" =1  bedeuted: Paket ist angekommen 
            await deleteOldestReceivedPackageIfCountIs50(db); // Bei 50 erfolgreich angelieferten Paketen wird das erste von den angelieferten gelöscht werden
            await db.closeConnection();
            await sendEmailsPromise();
            tuereClosed(); // Es wird überprüft, ob die Türe geschlossen ist, wenn nicht, wird der Besitzer benachrichtigt
        } 
        if (Way === 0) {  // Wenn das Paket nicht in die Box passt oder beschädigt ist
            const currentTime = getCurrentTime(); 
            const updateQuery = `UPDATE packages SET received = 3, timeStamp ="${currentTime.format('YYYY-MM-DD HH:mm:ss')}" WHERE paketNumber = ?`;
            await db.runQuery(updateQuery, [code]);
            await db.closeConnection();
            console.log('received auf 3 gesetzt.');  // "received" =1  bedeuted: Paket ist angekommen 
            await sendUnsuccessfulEmailsPromise();
            tuereClosed(); // Es wird überprüft, ob die Türe geschlossen ist, wenn nicht, wird der Besitzer benachrichtigt
        }
    } catch (error) {
        console.error(error.message);
    } 
}

function errorButtonPressed(){
    errorStatus = 1;
    console.log("Error Button wurde gedrückt");
  }

function waitForErrorButton() {
    // Starte das Timeout, wenn keine Eingabe erfolgt
    timeoutId = setTimeout(() => {
    console.log("Fehlertaster nicht betätigt");
    }, 60000);

    return new Promise((resolve, reject) => {

  
    
      clearTimeout(timeoutId); // Lösche das Timeout, wenn "Ja" eingegeben wurde
      if (errorStatus===1) {
        errorStatus = 0;
        resolve(1);
        console.log("Lukas lässt Hähnchen anbrennen, Fehlertaster betätigt");
      } else {
        resolve(0);
        waitForErrorButton();  // Starte das Programm erneut, wenn "Ja" nicht eingegeben wurde
      }
    });
  };

  
  

module.exports = {
    processPackageAndCloseDoor, errorButtonPressed
};

//==================================================================
//
// Datei: PackageProcessIfPackageNot.js 
//
// Beschreibung: In dieser Datei befindet sich die Funktion: processPackageIfNotExpected(db, code, answer) und die definition für die wrongPackages E-Mail
// Notizen: //
// Autor: Nils Melber
// Version: 1.0
// Stand: 10.01.24 19:30 Uhr
//==================================================================
const EmailSender = require('./emailSenderClass'); 
const { getCurrentTime } = require('./getCurrentTime');

const wrongPackages = new EmailSender(
    `<h1>Es wurde versucht ein nicht erwartetes Paket zuzustellen</h1>
   <p>Es wurde versucht ein nicht erwartetes Paket zuzustellen. Überprüfen Sie, ob Sie alle erwarteten Pakete korrekt eingetragen haben und ändern Sie es gegebenenfalls, 
    dass das Paket morgen zugestellt werden kann.</p>`,
    'Es wurde versucht ein nicht erwartetes Paket zuzustellen'
);

//===FUNKTION======================================================
// Name: processPackageIfNotExpected
// Beschreibung: Es wird geschaut, die Paketnummer bereits in der Datenbank vorhanden ist, je nach dem, wird der Datenbankeintrag verändert/ hinzugefügt und die oben beschriebene MAil gesendet
// Parameter: "db" ist die Datenbank, auf die zugegriffen wird 
// Parameter: "code",  ist die Zahlenkette die von dem Barcode scanner eingelesen wird.
// Parameter: "row" ist die Reihe in der Datenbank, die übergeben wurde, wenn das Paket schon in der Datenbank abgelegt ist ("received" !=0)
//==================================================================
async function processPackageIfNotExpected(db, code, row) {
    const sendWrongPackageEmailsPromise = () => {
        return new Promise((resolve, reject) => {
            wrongPackages.sendEmails()
                .then(() => resolve())
                .catch((e) => reject(e));
        });
    };

    try {
        if (row) {  // Wenn das Paket schon in der Tabelle vorhanden ist, aber received nicht auf 0 ist, wird nur der "received" Status geändert und die Mail gesendet 
            const currentTime = getCurrentTime();
            const updateQuery = `UPDATE packages SET received = 3, timeStamp = "${currentTime.format('YYYY-MM-DD HH:mm:ss')}" WHERE paketNumber = ?`;
            await db.runQuery(updateQuery, [code]);
            await db.closeConnection();
            console.log('received wurde auf 2 gesetzt und timeStamp aktualisiert.');
            await sendWrongPackageEmailsPromise();

        } else {   // Wenn das Paket noch nicht in der Tabelle vorhanden ist, wird es hinzugefügt und die Mail versendet
            const currentTime = getCurrentTime();
            const insertQuery = `INSERT INTO packages (paketNumber, description, timeStamp, expectedPacketID, received) VALUES (?, "WrongPackage", "${currentTime.format('YYYY-MM-DD HH:mm:ss')}", NULL, 2)`;
            await db.runQuery(insertQuery, [code]);
            await db.closeConnection();
            console.log('Paket wurde hinzugefügt und received auf 2 gesetzt.');
           // db.closeConnection();
            await sendWrongPackageEmailsPromise();
        }
    } catch (error) {
        console.error(error.message);
    } 
}

 module.exports = {
  processPackageIfNotExpected 
};

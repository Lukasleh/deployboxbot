function isCorrectMailAdress(email) {
    // Definiere eine einfache reguläre Ausdruck für die Überprüfung der E-Mail-Adresse
    var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    
    // Teste den übergebenen String gegen den regulären Ausdruck
    return emailRegex.test(email);
  }
  
  function isPosIntNumber(str) {
    // Definiere einen regulären Ausdruck für eine reine Zahlenfolge posiitiver ganzzahlen ist
    var numbersRegex =  /^[0123456789]+$/ ;// Aus: https://www.tutorials.de/threads/javascript-regex-fuer-zahlen.346216/
    
    // Teste den übergebenen String gegen den regulären Ausdruck
    return numbersRegex.test(str);
  }

  function isDescription(str) {
    // Definiere einen regulären Ausdruck für eine reine Zahlenfolge posiitiver ganzzahlen ist
    var letterRegex  = /^[A-Za-z\- ,.]+$/;// Aus: https://www.tutorials.de/threads/javascript-regex-fuer-zahlen.346216/
    
    // Teste den übergebenen String gegen den regulären Ausdruck
    return letterRegex .test(str);
  }
  function isRealName(name) {
    var nameRegex = /^[A-Za-z\- ]+$/;
    return nameRegex.test(name);
  }

  // // Beispielaufrufe
  // var zahl1 = "12345";
  // var zahl2 = "123abc";
  
  // console.log(zahl1 + " ist eine reine Zahlenfolge: " + istReineZahlenfolge(zahl1));
  // console.log(zahl2 + " ist eine reine Zahlenfolge: " + istReineZahlenfolge(zahl2));
  // // Beispielaufrufe
  // var email1 = "test@example.com";
  // var email2 = "ungueltige-email";
  
  // console.log(email1 + " ist gültig: " + istGueltigeEmail(email1));
  // console.log(email2 + " ist gültig: " + istGueltigeEmail(email2)); 

  module.exports = {
    isPosIntNumber,
    isCorrectMailAdress,
    isDescription,
    isRealName
  };
const express = require('express');
const router = express.Router();
const sqlite3 = require('sqlite3').verbose();
const path = require('path');

// Verwenden Sie wss, um Daten zu senden

const dbPath = path.resolve(__dirname, 'databaseSwProject.db');

const db = new sqlite3.Database(dbPath, sqlite3.OPEN_READWRITE, (err) => {
    if (err) return console.error(err.message);
    console.log("Connection successful");
});



let storedWsServer = null;

function getDataFromDatabase(){
  return new Promise((resolve, reject) => {
    const showLast5receivedPackagesSql = 'SELECT paketNumber, description, timeStamp FROM packages WHERE received = 1 ORDER BY expectedPacketID DESC LIMIT 5;';
    db.all(showLast5receivedPackagesSql, (err, rows) => {
      if (err) {
        console.error(err.message);
        reject(err);
        return;
      }
      resolve(rows);
    });
  });
}

router.get('/AllExpectedPackages', (req, res) => {
const showAllExpectedPackagesSql = 'SELECT paketNumber, description, timeStamp FROM packages WHERE received = 0 ORDER BY expectedPacketID DESC;';
  db.all(showAllExpectedPackagesSql, (err, rows) => {
    if (err) {
      console.error(err.message);
      res.status(500).send('Error getting data from database');
      return;
    }
    res.json({ data: rows });
  });
})

router.get('/AllReceivedPackages', (req, res) => {
  const showAllExpectedPackagesSql = 'SELECT paketNumber, description, timeStamp FROM packages WHERE received = 1 ORDER BY expectedPacketID DESC;';
    db.all(showAllExpectedPackagesSql, (err, rows) => {
      if (err) {
        console.error(err.message);
        res.status(500).send('Error getting data from database');
        return;
      }
      res.json({ data: rows });
    });
  })

  router.get('/AllFailedPackages', (req, res) => {
    const showAllExpectedPackagesSql = 'SELECT paketNumber, description, timeStamp FROM packages WHERE received = 2 ORDER BY expectedPacketID DESC;';
      db.all(showAllExpectedPackagesSql, (err, rows) => {
        if (err) {
          console.error(err.message);
          res.status(500).send('Error getting data from database');
          return;
        }
        res.json({ data: rows });
      });
    })

    router.get('/MailingList', (req, res) => {
      const showAllMails= 'SELECT adress, name FROM eMailAdress;';
        db.all(showAllMails, (err, rows) => {
          if (err) {
            console.error(err.message);
            res.status(500).send('Error getting data from database');
            return;
          }
          res.json({ data: rows });
        });
      })

router.get('/ExpectedPackages', (req, res) => {
  getDataFromDatabase()
    .then(rows => {
      console.log("Data:", rows);
      res.json({ data: rows });

    })
    .catch(err => {
      console.error(err);
      console.log("ERROR");
      res.status(500).send('Error getting data from database');
    });
});

function sendLast5receivedPackages(wsServer, MessageMQTT) {
  return new Promise((resolve, reject) => {
    console.log("Funktion wird aufgerufen!");
    

    // Speichern Sie wsServer, wenn es nicht null ist
    if (wsServer) {
      storedWsServer = wsServer;
    }

    getDataFromDatabase()
    .then(rows => {console.log("Daten MQTT", MessageMQTT );
        const data = JSON.stringify({ data: rows, mqttMessage: MessageMQTT });

        // Verwenden Sie storedWsServer anstelle von wsServer
        if (storedWsServer && storedWsServer.connections.length > 0) {
          storedWsServer.connections.forEach(connection => {
            if (connection.connected) {
              console.log(data);
              connection.sendUTF(data);
            }
            else{
              console.log("Websocket nicht offen");
            }
          });
        } else {
          console.error('Kein WebSocket-Server vorhanden oder keine Verbindungen');
        }
        resolve();
    });
  });
}

module.exports = { sendLast5receivedPackages , router};


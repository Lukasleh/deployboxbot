//==================================================================
//
// Datei: addToDatabase.js
//
// Beschreibung: In dieser Datei befindet sich dies Backend-Funktionen um die vom Client gesendeten Daten in die Datenbank zu schreiben
// Notizen: //
// Autor: Nils Melber, Lukas Lehmann
// Version: 1.0
// Stand: 07.01.24 18:00 Uhr
//==================================================================
const express = require('express');
const router = express.Router();
const sqlite3 = require('sqlite3').verbose();
const path = require('path');
const dbPath = path.resolve(__dirname, 'databaseSwProject.db');
const { sendLast5receivedPackages }  = require('./readFromDatabase.js');
const { isPosIntNumber } = require("./validation");
const { isCorrectMailAdress } = require("./validation");
const { isDescription } = require("./validation");
const { isRealName } = require("./validation");
const { getCurrentTime } = require('./getCurrentTime');

db = new sqlite3.Database(dbPath, sqlite3.OPEN_READWRITE, (err) => {
    if (err) return console.error(err.message);
    console.log("Connection successful");
})
router.use(function timeLog (req, res, next) {
    console.log('Time: ', Date.now())
    next()
})

router.post('/package', async (req, res) => {
    getCurrentTime
    const currentTime = getCurrentTime();
    const packagesql = `INSERT INTO packages (paketNumber, description, timeStamp, expectedPacketID, received) VALUES (?, ?,  "${currentTime.format('YYYY-MM-DD HH:mm:ss')}", NULL, 0)`;
    const { PackageNumber, description } = req.body;
    if (isPosIntNumber(PackageNumber)&&isDescription(description)) {
    const package = [PackageNumber, description];

    db.run(packagesql, package, async (err) => {
        if (err) {
          res.status(500).json({ error: err.message });
          return;
        }
        res.status(201).json({ success: "A new row has been created"  });
        console.log("Ein neues Paket wurde hinzugefügt");

        try {
            await sendLast5receivedPackages();
        } catch (error) {
            console.error('Fehler beim Senden der letzten 5 empfangenen Pakete:', error);
        }
    });
    } else{
        res.status(500).json({ error: err.message });
        console.error('Eingabe/übermittlung von Nummer, beschreibung Fehelerhaft, Name/Nummer ungültig');
        return; 
    }
});

router.post('/mail', (req, res) => {
    const mailsql= 'INSERT INTO eMailAdress (Adress, Name) VALUES(?,?) ' ;
    const { email, name} = req.body;
    if (isCorrectMailAdress(email)&&isRealName(name)) {
    const mail = [email, name];

    db.run(mailsql,[email, name],(err) => {
        if (err) {
          res.status(500).json({ error: err.message });
          console.log("Error User already added")
          return;
        }
        res.status(201).json({ success: "User added"  });
        console.log("New data (Mail user) has been created");
    });
    } else{
        res.status(500).json({ error: err.message });
        console.error('Eingabe/übermittlung von Name, E-Mail Fehelerhaft, Name, E-Mail ungültig');
        return;
    }
});

router.delete('/email/:adress', async (req, res) => {
    const { adress } = req.params;
    
    // Überprüfen, ob die Adresse gültig ist
    if (!isCorrectMailAdress(adress)) {
        res.status(500).json({ error: "Ungültige E-Mail-Adresse" });
        return;
    }
  
    const deleteEmailSql = 'DELETE FROM eMailAdress WHERE adress = ?';

    db.run(deleteEmailSql, [adress], function(err) {
        if (err) {
            res.status(500).json({ error: err.message });
            console.error('Löschen hat nicht funktioniert');
            return;
        }

        if (this.changes === 0) {
            res.status(500).json({ error: "E-Mail-Adresse nicht gefunden" });
            return;
        }
        else{
        res.status(201).json({ success: "E-Mail-Adresse wurde erfolgreich gelöscht" });
        console.log("E-Mail-Adresse wurde gelöscht");
        }
    });
});

module.exports = router;
//==================================================================
//
// Datei: databaseClass.js 
//
// Beschreibung: In dieser Datei befindet sich die Klassendefiniotin der Datenbank Klasse
// Notizen: //
// Autor: Nils Melber
// Version: 1.0
// Stand: 10.01.24 19:15 Uhr
//==================================================================
const sqlite3 = require('sqlite3').verbose();
class Database {
    constructor(dbPath) {
        this.dbPath = dbPath;
        this.db = new sqlite3.Database(this.dbPath, sqlite3.OPEN_READWRITE, (err) => {
            if (err) return console.error(err.message);
            console.log("Connection successful");
        });
    }
    async closeConnection() {
        return new Promise((resolve, reject) => {
            this.db.close((closeErr) => {
                if (closeErr) {
                    console.error(closeErr.message);
                    reject(closeErr);
                } else {
                    console.log('Datenbankverbindung geschlossen.');
                    resolve();
                }
            });
        });
    }
    
    async query(sql, params) {
        return new Promise((resolve, reject) => {
            this.db.all(sql, params, (err, rows) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(rows);
                    console.log('Datenbankeintrag erfolgrreich.');
                }
            });
        });
    }
    async queryOne(sql, params) {
        return new Promise((resolve, reject) => {
            this.db.get(sql, params, (err, row) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(row);
                    console.log('Datenbankeintrag erfolgrreich.');
                }
            });
        });
    }
    async runQuery(sql, params) {
        return new Promise((resolve, reject) => {
            this.db.run(sql, params, function (err) {
                if (err) {
                    reject(err);
                } else {
                    resolve({ lastID: this.lastID, changes: this.changes });
                    console.log('Datenbankeintrag erfolgrreich.');
                }
            });
        });
    }
}

module.exports = Database;
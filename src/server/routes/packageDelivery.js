//==================================================================
//
// Datei: PackageDelivery.js
//
// Beschreibung: In dieser Datei befindet sich die Funktion: packageDelivery(code) und  checkCodeAndOpenDoor(code)
// Notizen: //
// Autor: Nils Melber
// Version: 1.0
// Stand: 10.01.24 19 Uhr
//==================================================================
const { processPackageAndCloseDoor } = require("./PackageProcessIfPackagefound");
const { processPackageIfNotExpected } = require("./PackageProcessIfPackageNot");
const { isPosIntNumber } = require("./validation"); 
const readline = require("readline-sync");
const path = require('path');
const dbPath = path.resolve(__dirname, 'databaseSwProject.db');
const Database = require("./databaseClass"); //

//===FUNKTION======================================================
// Name: checkCodeAndOpenDoor
// Beschreibung: Sucht in der DB ob das Paket erwartet wird und ruft je nach dem die darauf folgende Funktion auf
// Parameter: "code",  ist die Zahlenkette die von dem Barcode scanner eingelesen wird.
//==================================================================
async function checkCodeAndOpenDoor(code) {
  const db = new Database(dbPath);
  const query = "SELECT * FROM packages WHERE paketNumber = ?"; // SQL-Abfrage, um den Zahlencode in der Datenbank zu suchen

  try {
    const row = await db.queryOne(query, [code]); // gibt die Reihe aus, in der der code gefunden wurd, falls er vorhanden ist

    if (row && row.received === 0) { // wenn der code gefunden ist, und der "received" Status = null (erwartet) gesetzt ist
      console.log("Türe öffnen...");
      
      await processPackageAndCloseDoor(db, code);
    } else {  // wenn der Zahlencode wurde nicht gefunden
      console.log("Barcode nicht als erwartetes Paket gelistet");
      await processPackageIfNotExpected(db, code, row);
    }
  } catch (error) {
    console.error(error.message);
  }
}

//===FUNKTION======================================================
// Name: packageDelivery
// Beschreibung: Überprüft, ob der "code" eine Zahlenkette ist und ruft die checkCodeAndOpenDoor Funktion auf. Wenn es keine Zahlenkette ist wird das Programm beendet
// Parameter: "code",  ist die Zahlenkette die von dem Barcode scanner eingelesen wird.
//==================================================================
function packageDelivery(code) {
  if (isPosIntNumber(code)) { 
    console.log("Code wurde übergeben",code);// Code ist eine Zahlenkette
    checkCodeAndOpenDoor(code);
  } else {  // Code ist keine Zahlenkette
    console.log("Der eingegebene Code ist keine gültige Zahlenkette.");
    return;
  }
}

module.exports = {packageDelivery};
// Nutzereingabe für den Zahlencode
//const code = parseInt(
  //readline.question("Bitte geben Sie den Zahlencode ein: ")
//);
//packageDelivery(code);

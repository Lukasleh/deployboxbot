const express = require('express');
const fs = require('fs');
const router = express.Router();

router.post('/set', (req, res) => {
    const { startHour, endHour } = req.body;

    // Überprüfen, ob die Stundenangaben gültig sind
    if (startHour < 0 || startHour > 23 || endHour < 0 || endHour > 23) {
        res.status(400).json({ message: 'Ungültige Stundenangabe.' });
        return;
    }

    // Die Zeiten in einer Textdatei speichern
    fs.writeFile('time.config', `Startzeit: ${startHour}, Endzeit: ${endHour}`, (err) => {
        if (err) {
            console.error(err);
            res.status(500).json({ message: 'Es gab ein Problem beim Speichern der Zeiten.' });
        } else {
            res.status(200).json({ message: 'Zeiten erfolgreich gespeichert!' });
        }
    });
});


router.get('/get', (req, res) => {
    fs.readFile('time.config', 'utf8', (err, data) => {
        if (err) {
            console.error(err);
            res.status(500).json({ message: 'Es gab ein Problem beim Abrufen der Zeiten.' });
        } else {
            const [startHour, endHour] = data.replace('Startzeit: ', '').replace('Endzeit: ', '').split(', ');
            res.status(200).json({ startHour, endHour });
        }
    });
});

module.exports = router;
const mqtt = require('mqtt');
const { sendLast5receivedPackages } = require('./readFromDatabase'); // Pfad zu Ihrer Funktion
const { packageDelivery } = require("./PackageDelivery.js");

function startMqtt(wsServer) {
  return new Promise((resolve, reject) => {
    const host = 'localhost';
    const port = 1883;
    const mqttClient = mqtt.connect(`mqtt://${host}:${port}`);

    if (wsServer) {
      storedWsServer = wsServer;
    }

    mqttClient.on('connect', () => {
      console.log('Connected to MQTT broker');
      mqttClient.subscribe('Inputs', (err) => {
        if (err) {
          console.error('Failed to subscribe to topic:', err);
          reject(err);
        }
      });

      mqttClient.subscribe('Barcode', (err) => {
        if (err) {
          console.error('Failed to subscribe to second topic:', err);
          reject(err);
        }
      });
    

    mqttClient.subscribe('ErrorButton', (err) => {
      if (err) {
        console.error('Failed to subscribe to second topic:', err);
        reject(err);
      }
    });
  });
 
   

    mqttClient.on('error', (err) => {
      console.error('MQTT error:', err);
      reject(err);
    });

    mqttClient.on('message', (topic, message) => {
      try {
        if(topic === 'Inputs')
        {console.log('Received message on topic:', topic);
        let messageStr = message.toString();
        console.log('Message:', messageStr);

        let parsedMessage = JSON.parse(message.toString());
        let parsedMessage2= JSON.parse(parsedMessage);
        console.log("parsed message", parsedMessage2);
        let statusLightValue = parsedMessage2.statusLight; 

        // Rufen Sie sendLast5receivedPackages auf, wenn eine Nachricht empfangen wird
        sendLast5receivedPackages(storedWsServer, statusLightValue);
        resolve(statusLightValue);
      }

        else if(topic === 'Barcode')
        {console.log('Received message on topic:', topic);
        let consoleCode = message.toString();
        let parsedMessage = JSON.parse(consoleCode);
        let code = parsedMessage.PackageNumber;
        console.log("Message MQTT Barcode",parsedMessage);
        packageDelivery(code);
      }
      else if(topic === 'ErrorButton')
      {
        errorButtonPressed();
      }
        
      } catch (err) {
        console.error('Subscription error:', err);
        reject(err);
      }
    });
  });
}

module.exports = startMqtt;
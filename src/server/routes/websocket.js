const http = require('http');
const WebSocketServer = require('websocket').server;
const uuidv4 = require("uuid").v4;

async function startWebSocketServer() {
    const webSocketServerPort = 6500;
    const server = http.createServer();

    return new Promise((resolve, reject) => {
        server.listen(webSocketServerPort, () => {
            console.log('WebSocket Server is listening to Port 6500');
            const wsServer = new WebSocketServer({
                httpServer: server,
                autoAcceptConnections: true
            });

            const clients = {};

            // ClientID nutzen
            const getUniqueID = () => {
                const uuid = uuidv4();
                console.log("Meine uuid: ", uuid);
                return uuid;
            };

            wsServer.on('request', function (request) {
                var userID = getUniqueID();
                const connection = request.accept(null, request.origin);
                clients[userID] = connection;

                connection.on('message', function(message) {
                    if (message.type === 'utf8') { // accept only text
                        console.log('Received Message: ', message.utf8Data);
                        // broadcasting message to all connected clients
                        for(key in clients) {
                            clients[key].sendUTF(message.utf8Data);
                        }
                    }
                });

                connection.on('close', function(connection) {
                    console.log((new Date()) + " Peer " + userID + " disconnected.");
                    // remove user from the list of connected clients
                    delete clients[userID];
                });
            });

            wsServer.on('error', (err) => {
                reject(err);
            });

            resolve(wsServer);
        });
    });
}

module.exports = startWebSocketServer;

//==================================================================
//
// Datei: emailSenderClass.js 
//
// Beschreibung: In dieser Datei befindet sich die Klassendefiniotin der Email Klasse
// Notizen: //
// Autor: Nils Melber
// Version: 1.0
// Stand: 10.01.24 19:15 Uhr
//==================================================================
const Database = require('./databaseClass');
const nodeMailer = require('nodemailer');
const path = require('path');
const dbPath = path.resolve(__dirname, 'databaseSwProject.db');
const config = require('./config.json');
class EmailSender {
    constructor(html, subject) {
        this.html = html;
        this.subject = subject;
    }
    async sendEmails() {
        const db = new Database(dbPath);

        try {
            await db.runQuery('CREATE TABLE IF NOT EXISTS eMailAdress (adress TEXT)'); // Stelle sicher, dass die Tabelle existiert
            const query = 'SELECT adress FROM eMailAdress';
            const rows = await db.query(query, []);
            const transporter = nodeMailer.createTransport(config.emailConfig);

            for (const row of rows) {
                const toAddress = row.adress;

                if (!toAddress) {
                    console.error(`Invalid email address found: ${toAddress}`);
                    continue;
                }

                try {
                    const info = await transporter.sendMail({
                        from: config.emailConfig.auth.user,
                        to: toAddress,
                        subject: this.subject,
                        html: this.html,
                    });

                    console.log(`Message sent to ${toAddress}: ${info.messageId}`);
                } catch (error) {
                    console.error(`Error sending email to ${toAddress}: ${error.message}`);
                }
            }
        } catch (error) {
            console.error(`Error querying database: ${error.message}`);
        } finally {
           await db.closeConnection();
        }
    }
}



module.exports = EmailSender;


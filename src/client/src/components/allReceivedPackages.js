import React, { useEffect, useState } from 'react';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Typography, Button } from '@mui/material';
import './Styles/boxBotPanel.css';

function ReceivedPackagesTable() {
    const [data, setData] = useState([]);

    const fetchData = () => {
        fetch('/api/read/AllReceivedPackages', {
            method: 'GET',
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then((result) => result.json())
        .then((result) => {
            setData(result.data);  
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    }

    useEffect(() => {
        fetchData();
    }, []); 

    return (
        <div id='Container' className='TablePackages'>
            <Typography variant="h4" component="div" gutterBottom>
                Paketübersicht
            </Typography>
            <Button variant="contained" color="primary" onClick={fetchData}>
                Aktualisieren
            </Button>
            <TableContainer component={Paper} >
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Paketnummer</TableCell>
                            <TableCell>Beschreibung</TableCell>
                            <TableCell>Zeitstempel</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.map((row, index) => (
                            <TableRow key={index}>
                                <TableCell>{row.paketNumber}</TableCell>
                                <TableCell>{row.description}</TableCell>
                                <TableCell>{row.timeStamp}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
}

export default ReceivedPackagesTable;
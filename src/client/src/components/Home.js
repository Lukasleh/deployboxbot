//Home.js
import React from 'react';
import Grid from '@mui/material/Grid';
import Mail from './mailBox';
import PackageManager from './PackageManagerBox';
import PackagesTable from './lastPackagesBox';
import GPIO from './GPIO';
import TimeInput from './timeInput';
import './Styles/Home.css'


const Home = () => {

 


  return (
    <div id="Home">
        <Grid container spacing={2} id="grid">
          <Grid item lg={8} md={6} xs={12}>
            <PackagesTable/>
          </Grid>
          <Grid item lg={4} md={6} xs={12}>
          <GPIO onStatusDoorChange={(newStatusDoor) => console.log(`Neuer Status Tür: ${newStatusDoor}`)} 
                onStatusBoxChange={(newStatusBox) => console.log(`Neuer Status Box: ${newStatusBox}`)} />
          </Grid>
          <Grid item lg={4}md={4} xs={12}>
            <Mail/>
          </Grid>
          <Grid item lg={4} md={4} xs={12}>
            <PackageManager/>
          </Grid>
          <Grid item lg={4} md={4} xs={12}>
            <TimeInput/>
          </Grid>
        </Grid>
    </div>
    );
}

export default Home;

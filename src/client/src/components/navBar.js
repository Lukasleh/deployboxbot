import { styled } from '@mui/system';
import { Link, BrowserRouter, Route, Switch } from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import {Tab, Tabs, useMediaQuery, AppBar, Toolbar, IconButton, Typography, Drawer, List, ListItem, ListItemText, Button } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import './Styles/navBar.css';
import Home from './Home';
import Logs from './Logs';
import ExpectedPackagesTable from './allExpectedPackages';
import ReceivedPackagesTable from './allReceivedPackages';
import FailedPackagesTable from './allFailedPackages';
import MailSettings from './mailSettings';
import { Routes } from 'react-router-dom/dist/umd/react-router-dom.development';

const NavbarComponent = ({ logo, softwareVersion, teamName}) => {
    const isSmallScreen = useMediaQuery(theme => theme.breakpoints.down('sm'));
    const [state, setState] = useState({ left: false });
    const [activeTab, setActiveTab] = useState(0);
    const [timeStamp, setTimeStamp] = useState(new Date().toLocaleTimeString());

    useEffect(() => {
        const timer = setInterval(() => {
            setTimeStamp(new Date().toLocaleTimeString());
        }, 1000);
        return () => clearInterval(timer);
    }, []);

    const toggleDrawer = (anchor, open) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
        setState({ ...state, [anchor]: open });
    };

    const list = (anchor) => (
        <div
            className="list"
            role="presentation"
            onClick={toggleDrawer(anchor, false)}
            onKeyDown={toggleDrawer(anchor, false)}
        >
            <List>
                <ListItem button component={Link} to="/Home" onClick={() => setActiveTab(0)}>
                    <ListItemText primary="Home" />
                </ListItem>
                <ListItem button component={Link} to="/Logs" onClick={() => setActiveTab(1)}>
                    <ListItemText primary="Logs" />
                </ListItem>
                <ListItem button component={Link} to="/ReceivedPackagesTable" onClick={() => setActiveTab(2)}>
                    <ListItemText primary="Erhaltene Pakete" />
                </ListItem>
                <ListItem button component={Link} to="/ExpectedPackagesTable" onClick={() => setActiveTab(3)}>
                    <ListItemText primary="Erwartete Pakete" />
                </ListItem>
                <ListItem button component={Link} to="/FailedPackagesTable" onClick={() => setActiveTab(3)}>
                    <ListItemText primary="Zustellungsversuche" />
                </ListItem>
                <ListItem button component={Link} to="/MailSettings" onClick={() => setActiveTab(4)}>
                    <ListItemText primary="Mail-Einstellungen" />
                </ListItem>
            </List>
        </div>
    );

    return (
        <div className='navbar'>
            <BrowserRouter>
            {isSmallScreen ? (
                // Burger-Menü für kleine Bildschirme
                <>
                    <AppBar position="static" >
                        <Toolbar >
                            <IconButton edge="start" className="menuButton" color="inherit" aria-label="menu" onClick={toggleDrawer('left', true)}>
                                <MenuIcon />
                            </IconButton>
                            <img src={logo} alt="Logo" className='Logo'/>
                            <Typography variant="h6" id="softwareVersion">
                                <p>{softwareVersion} | Uhrzeit: {timeStamp} Uhr | {teamName}</p>
                            </Typography>
                        </Toolbar>
                    </AppBar>
                    
                    <Drawer anchor={'left'} open={state['left']} onClose={toggleDrawer('left', false)}>
                        {list('left')}
                    </Drawer>
                </>
            ) : (
                // Normale AppBar für größere Bildschirme
                <AppBar position="static" >    
                        <AppBar position="static" color='secondary'>
                            <Toolbar className='bigToolBar'>
                                <img src={logo} alt="Logo" className='Logo'/>
                                <Tabs
                                    value={activeTab}
                                    onChange={(event, newValue) => setActiveTab(newValue)}
                                >
                                    <Tab component={Link} to="/Home" label="Home" />
                                    <Tab component={Link} to="/Logs" label="Logs" />
                                    <Tab component={Link} to="/ReceivedPackagesTable" label="Angelieferte Pakete" />
                                    <Tab component={Link} to="/ExpectedPackagesTable" label="Erwartete Pakete" />
                                    <Tab component={Link} to="/FailedPackagesTable" label="Zustellungsversuche" />
                                    <Tab component={Link} to="/MailSettings" label="Mail-Einstellungen" />
                                </Tabs>
                            </Toolbar>
                            <Typography variant="h6" id="softwareVersion">
                                <p>{softwareVersion} | Uhrzeit: {timeStamp} Uhr | {teamName}</p>
                            </Typography>
                        </AppBar>
                </AppBar>
            )}
                <Routes>
                    <Route path="/Home" element={<Home />} />
                    <Route path="/Logs" element={<Logs />} />
                    <Route path="ReceivedPackagesTable" element={<ReceivedPackagesTable />} />
                    <Route path="ExpectedPackagesTable" element={<ExpectedPackagesTable />} />
                    <Route path="FailedPackagesTable" element={<FailedPackagesTable />} />
                    <Route path="MailSettings" element={<MailSettings />} />
                </Routes>
            </BrowserRouter>
        </div>
    );
};

export default NavbarComponent;
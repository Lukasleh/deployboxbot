import React, { useState, useEffect } from "react";
import "./Styles/GPIO.css";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import FormHelperText from "@mui/material/FormHelperText";
import { Button, TextField, Switch } from "@mui/material";

const GPIO = ({ onStatusDoorChange, onStatusBoxChange }) => {
  const [statusError, setStatusError] = useState(1);
  const [statusBell, setStatusBell] = useState(0);
  const [StatusDoor, setStatusDoor] = useState(0);
  const [statusBox, setStatusBox] = useState(0);
  const [jsonData, setJsonData] = useState(null); //Test für Json Datei
  const client = new WebSocket("ws://localhost:6500");

  useEffect(() => {
    client.onopen = () => {
      console.log("WebSocket Client Connected");
    };

    client.onmessage = (message) => {
      console.log("Received data55:", message.data);
      console.log("message von der GPIO", message);

      let data = JSON.parse(message.data);

      setJsonData(data);

      console.log("Nach dem parsen", data);

      const jsonString = '{"statusLight": "1"}';

      if (data.statusDoor !== undefined) {
        setStatusDoor(data.statusDoor);
      }
    };
  }, []);
  useEffect(() => {
    console.log("Status Error:", statusError);
  }, [statusError]);

  useEffect(() => {
    console.log("Die JSSSOOONN: ", jsonData);
    if (jsonData) {
      setStatusError(jsonData.mqttMessage);
    }
    // if (jsonData) {
    //   const parsedData = JSON.parse(jsonData);
    //   if (parsedData && parsedData['statusLight'] !== undefined) {
    //     console.log('Hallo', parsedData);
    //     setStatusError(parsedData.statusLight);
    //   }
    // }
  }, [jsonData]);

  useEffect(() => {
    console.log("Updated statusError: ", statusError);
  }, [statusError]);

  const handleButtonPressBoxOpen = () => {
    if (statusBox !== 1) {
      setStatusBox(1);
      onStatusBoxChange(1);
    }
  };
  
  const handleButtonPressBoxClose = () => {
    if (statusBox !== 0) {
      setStatusBox(0);
      onStatusBoxChange(0);
    }
  };

  const handleSwitchChangeDoor = (event) => {
    const newStatusDoor = event.target.checked ? 1 : 0;
    setStatusDoor(newStatusDoor);
    onStatusDoorChange(newStatusDoor);
  };

  return (
    <div id="Container">
      <div id="Container-GPIOs">
        <h4>Status Eingänge</h4>
        <div className="Row-GPIOs">
          <p1 className="normal-text">Fehler</p1>
          <div className={`light ${statusError === "1" ? "an" : ""}`}> </div>
        </div>
        <div className="Row-GPIOs">
          <p1 className="normal-text">Klingel </p1>
          <div className={`light ${StatusDoor === 1 ? "an" : ""}`}> </div>
        </div>
        <h4>Status Ausgänge</h4>
        <div className="Row-GPIOs">
          <p1 className="normal-text">Boxöffner</p1>
          <Button
            variant="contained"
            style={{ backgroundColor: "green", color: "white", margin: "10px" }}
            onClick={handleButtonPressBoxOpen}
          >
            Box öffnen
          </Button>
          <Button
            variant="contained"
            style={{
              backgroundColor: "crimson",
              color: "white",
              margin: "10px",
            }}
            onClick={handleButtonPressBoxClose}
          >
            Box schließen
          </Button>
        </div>
        <div className="Row-GPIOs">
          <p1 className="normal-text">Türöffner</p1>
          <Switch defaultChecked onChange={handleSwitchChangeDoor} />
        </div>
        
      </div>
    </div>
  );
};

export default GPIO;

import React, { useState, useEffect } from 'react';
import './Styles/boxBotPanel.css'
import FormControl from '@mui/material/FormControl'
import FormLabel from '@mui/material/FormLabel'
import FormHelperText from '@mui/material/FormHelperText'
import { Button, TextField } from '@mui/material';

function TimeInput() {
    const [startHour, setStartHour] = useState("");
    const [endHour, setEndHour] = useState("");
    const [submitStatus, setSubmitStatus] = useState(null);
    const [error, setError] = useState({startHour: false, endHour: false});
    const [savedTime, setSavedTime] = useState("");

    const validateInput = () => {
        let isValid = true;
        if (startHour < 0 || startHour > 23) {
            setError(prevState => ({...prevState, startHour: true}));
            isValid = false;
        }
        if (endHour < 0 || endHour > 23) {
            setError(prevState => ({...prevState, endHour: true}));
            isValid = false;
        }
        return isValid;
    }

    const fetchSavedTime = () => {
        fetch('/api/Time/get')
            .then(response => response.json())
            .then(data => {
                const { startHour, endHour } = data;
                setSavedTime(`Aktuell gesetzte Zeit: ${startHour} Uhr bis ${endHour} Uhr`);
            })
            .catch(error => {
                console.error('Error:', error);
            });
    }

    useEffect(() => {
        fetchSavedTime();
    }, []);

    const handleSubmit = (event) => {
        event.preventDefault();

        if (!validateInput()) {
            return;
        }

        setSubmitStatus("Laden...");

        fetch('/api/Time/set', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({startHour, endHour}),
        })
        .then(response => {
            if (response.ok) {
                console.log('Success:', response);
                // Ladescreen ausblenden
                setSubmitStatus("Zeiten erfolgreich gespeichert!");
                fetchSavedTime();
            } else {
                console.error(`HTTP error! status: ${response.status}`);
                // Ladescreen ausblenden
                setSubmitStatus("Fehler! Es gab ein Problem beim Speichern der Zeiten.");
            }
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    }

    const resetForm = () => {
        setStartHour("");
        setEndHour("");
        setError({startHour: false, endHour: false});
        setSubmitStatus(null);
    }

    return (
        <div id='Container'>
            {submitStatus=== null ? (
        <FormControl onSubmit={handleSubmit}>
          <FormLabel>Zeiten speichern</FormLabel>
          <TextField 
            error={error.startHour}
            helperText={error.startHour ? "Bitte geben Sie eine gültige Startzeit ein (0-23)" : ""}
            label="Startzeit" 
            type="number" 
            min="0" 
            max="23" 
            color='primary'  
            value={startHour} 
            onChange={e => setStartHour(e.target.value)}
          />
          <TextField 
            error={error.endHour}
            helperText={error.endHour ? "Bitte geben Sie eine gültige Endzeit ein (0-23)" : ""}
            label="Endzeit" 
            type="number" 
            min="0" 
            max="23" 
            color='primary'  
            value={endHour} 
            onChange={e => setEndHour(e.target.value)}
          />
          <Button type="submit" variant="contained" color="primary" onClick={handleSubmit} disabled={!startHour || !endHour}>Speichern</Button>
          <FormHelperText>Start- und Endzeit für den Zeitraum</FormHelperText>
        </FormControl>
           ): (
                <div>
                     {submitStatus}
                <Button variant="contained" color="primary" onClick={resetForm}>Neue Zeiten</Button>
                </div>
            )}
            <p>{savedTime}</p>
        </div>
    );
}

export default TimeInput;
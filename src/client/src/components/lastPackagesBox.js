import React, { useEffect, useState } from 'react';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Typography } from '@mui/material';
import './Styles/boxBotPanel.css';

function PackagesTable() {
    const [data, setData] = useState([]);

    useEffect(() => {

        fetch('/api/read/ExpectedPackages', {
            method: 'GET',
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then(response => response.json())
                    .then(data => {
                        
                        console.log("Data:", data);
                        setData(data.data);

                    })
                    .catch(error => {
                        console.error('There was an error55!', error);
                    });


        
        const client = new WebSocket('ws://localhost:6500');
        client.onopen = () => {
            console.log('WebSocket Client Connectedhier');
        client.send('/showAllreceivedPackages');
    };
    
        client.onmessage = (message) => {
            console.log("Nils der kek",message.data)
            new Promise((resolve, reject) => {
                try {
                    const result = JSON.parse(message.data);
                    if (result.data) {
                        resolve(result.data);
                    } else if (result.error) {
                        reject('There was an error!', result.error);
                    }
                } catch (error) {
                    reject('Invalid JSON:', message.data);
                }
            })
            .then(data => {
                setData(data);
                console.log(data);
            })
            .catch(error => {
                console.error(error);
            });
        };
    }, []);

    return (
        <div id='Container' className='TablePackages'>
            <Typography variant="h4" component="div" gutterBottom>
                Deine letzten 5 Anlieferungen
            </Typography>
            <TableContainer component={Paper} >
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Paketnummer</TableCell>
                            <TableCell>Beschreibung</TableCell>
                            <TableCell>Zeitstempel</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.map((row, index) => (
                            <TableRow key={index}>
                                <TableCell>{row.paketNumber}</TableCell>
                                <TableCell>{row.description}</TableCell>
                                <TableCell>{row.timeStamp}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
}

export default PackagesTable;
import React, { useState } from 'react';
import './Styles/boxBotPanel.css'
import FormControl from '@mui/material/FormControl'
import FormLabel from '@mui/material/FormLabel'
import FormHelperText from '@mui/material/FormHelperText'
import { Button, TextField } from '@mui/material';

function Mail() {
    const [email, setEmail] = useState("");
    const [name, setName] = useState("");
    const [emailError, setEmailError] = useState(false);
    const [submitStatus, setSubmitStatus] = useState(null);

    const emailRegex = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;

    const handleSubmit = (event) => {
        event.preventDefault();
    
        if (!emailRegex.test(email)) {
            setEmailError(true);
            return;
        } 
    
        // Ladescreen anzeigen
        setSubmitStatus("Laden...");
    
        // API-Aufruf durchführen
        fetch('/api/edit/mail', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ email, name }),
        })
        .then(response => {
            if (response.ok) {
                console.log('Success:', response);
                // Ladescreen ausblenden
                setSubmitStatus("Vielen Dank für Ihre Anmeldung!");
            } else {
                console.error(`HTTP error! status: ${response.status}`);
                // Ladescreen ausblenden
                setSubmitStatus("Fehler! Es gab ein Problem beim Senden Ihrer E-Mail.");
            }
        })
        .catch(error => {
            console.error(error);
            // Ladescreen ausblenden
            setSubmitStatus(null);
        });
    };
    
    const resetForm = () => {
        setEmail("");
        setName("");
        setEmailError(false);
        setSubmitStatus(null);
    }

    return (
        <div id='Container'>
        {submitStatus === null ? (
            <form>
            <FormControl>
              <FormLabel>Benachrichtigungs-Liste</FormLabel>
              <TextField 
              error={emailError} helperText={emailError ? "Ungültige E-Mail-Adresse" : ""} 
              label="Email" type="text" color='primary' value={email} 
                onChange={e => {
                    setEmail(e.target.value);
                    setEmailError(false);
                }}/>
              <TextField label="Name" type="text" color='primary'  value={name} onChange={e => setName(e.target.value)}/>
              <Button type="submit" variant="contained" color="primary" onClick={handleSubmit} disabled={!email || !name}>Submit</Button>
              <FormHelperText>Hier wird eingetragen, wer im Falle einer Anlieferung o.ä. Ereignisses per Mail benachrichtigt wird</FormHelperText>
            </FormControl>
            </form>
        ) : (
            <div>
                {submitStatus}
                <Button variant="contained" color="primary" onClick={resetForm}>Neue E-Mail eingeben</Button>
            </div>
        )}
        </div>
    );
}

export default Mail;
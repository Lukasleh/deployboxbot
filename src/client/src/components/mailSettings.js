import React, { useEffect, useState } from 'react';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Typography, Button } from '@mui/material';
import './Styles/boxBotPanel.css';

function EmailSettings() {
    const [data, setData] = useState([]);

    const fetchData = () => {
        fetch('/api/read/MailingList', {
            method: 'GET',
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then((result) => result.json())
        .then((result) => {
            setData(result.data);  
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    }

    const deleteEmail = (email) => {
        fetch(`/api/edit/email/${email}`, {
            method: 'DELETE',
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then(() => {
            fetchData();
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    }
    useEffect(() => {
        fetchData();
    }, []); 

    return (
        <div id='Container' className='TablePackages'>
            <Typography variant="h4" component="div" gutterBottom>
                Paketübersicht
            </Typography>
            <Button variant="contained" color="primary" onClick={fetchData}>
                Aktualisieren
            </Button>
            <TableContainer component={Paper} >
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Paketnummer</TableCell>
                            <TableCell>Beschreibung</TableCell>
                            <TableCell>Aktion</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.map((row, index) => (
                            <TableRow key={index}>
                                <TableCell>{row.adress}</TableCell>
                                <TableCell>{row.name}</TableCell>
                                <TableCell>
                                    <Button variant="contained" color="secondary" onClick={() => deleteEmail(row.adress)}>
                                        Löschen
                                    </Button>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
}

export default EmailSettings;
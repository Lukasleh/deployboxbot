import React, { useState } from 'react';
import './Styles/boxBotPanel.css'
import FormControl from '@mui/material/FormControl'
import FormLabel from '@mui/material/FormLabel'
import FormHelperText from '@mui/material/FormHelperText'
import { Button, TextField } from '@mui/material';

function PackageManager() {
    const [PackageNumber, setPackageNumber] = useState("");
    const [description, setDescription] = useState("");
    const [submitStatus, setSubmitStatus] = useState(null);

    const handleSubmit = (event) => {
        event.preventDefault();

        // Ladescreen anzeigen
        setSubmitStatus("Laden...");

        fetch('/api/edit/package', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({PackageNumber, description}),
        })
        .then(response => {
            if (response.ok) {
                console.log('Success:', response);
                // Ladescreen ausblenden
                setSubmitStatus("Paket Erfolgreich hinzugefügt!");
            } else {
                console.error(`HTTP error! status: ${response.status}`);
                // Ladescreen ausblenden
                setSubmitStatus("Fehler! Es gab ein Problem beim hinzufügen des Pakets.");
            }
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    }

        const resetForm = () => {
            setPackageNumber("");
            setDescription("");
            setSubmitStatus(null);
        }


    return (
        <div id='Container'>
            {submitStatus=== null ? (
        <FormControl onSubmit={handleSubmit}>
          <FormLabel>Neues Paket</FormLabel>
          <TextField label="Paket-Nummer" type="text" color='primary'  value={PackageNumber} onChange={e => setPackageNumber(e.target.value)}/>
          <TextField label="Beschreibung" type="text" color='primary'  value={description} onChange={e => setDescription(e.target.value)}/>
          <Button type="submit" variant="contained" color="primary" onClick={handleSubmit} disabled={!PackageNumber}>Paket Hinzufügen</Button>
          <FormHelperText>Paketnummer und Beschreibung für neues Paket</FormHelperText>
        </FormControl>
           ): (
                <div>
                     {submitStatus}
                <Button variant="contained" color="primary" onClick={resetForm}>Neues Paket</Button>
                </div>
            )
            }
        </div>
    );
}

export default PackageManager;
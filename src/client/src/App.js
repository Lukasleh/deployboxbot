import logo from './media/LogoKlein.png';
import './App.css';
import Mail from './components/mailBox';
import PackageManager from './components/PackageManagerBox';
import PackagesTable from './components/lastPackagesBox';
import NavbarComponent from './components/navBar';
import { ThemeProvider } from '@mui/material/styles';
import theme from './theme';
import { Grid } from '@mui/material';
import GPIO from './components/GPIO';


const softwareVersion = "Softwareversion: 1.0.0";
const teamName = "Gruppe 32 BoxBot"



function App() {

  return (
    <ThemeProvider theme={theme}>
    <div className="App">
      <NavbarComponent logo={logo} softwareVersion={softwareVersion} teamName={teamName}/>
    </div>
    </ThemeProvider>
  );
}

export default App;

// theme.js

import { createTheme } from '@mui/material/styles';
import { amber, orange , cyan} from '@mui/material/colors';

const theme = createTheme({
  palette: {
    primary: amber,
    secondary: {
      main: '#80705a',
    },
  },
  // Weitere Theme-Einstellungen hier
  components:{
    MuiButton:{
      styleOverrides:{
        root:{
          borderRadius: 15,       //aplies on all MuiButtons
        },
        typography: {
        allVariants: {
            color: '#552af1' // Setzen Sie hier Ihre gewünschte Schriftfarbe
        }
    },
        outlinedPrimary:{       //aplies only to all outlinedPrimary Buttons
          borderRadius: 50,
          backgroundColor: '#000000'
        }
      }
    }
  }
});

export default theme;
